﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)K!!!*Q(C=\&gt;4"L&gt;J!&amp;)8B1R1JW@)+C#*;O#V11$;U=&amp;NAS^)NH!YC7K#&amp;WQ)NU),T?XTTR#)"+=K4XO,:8$"H\*G0]7"J&lt;&amp;_FTZK@&lt;6]?&lt;P@N]`+;VY-(\@.@WZ@2(L8@$@%P\8`W0^PU=0NI`WB`X_X0F`@J_:`A&lt;4&gt;J-[255;;3CP(LZC)P]C)P]C)P=J/&lt;X/1G.\H*ETT*ETT*ETT*ATT)ATT)ATT)RUYO=J',H&amp;EJ"C]'+DIN/CB/BK,CL@!5HM*4?0CKQF.Y#E`B+4S=IM*4?!J0Y3E]8+&lt;#5XA+4_%J0(1V*$6W=DS&amp;B_ZF0-:D0-:D0!QJYT%!-ZDJW(1#1[&lt;2("C0]2A0BT)?YT%?YT%?GG5]RG-]RG-]8$*GR5/T\/2Y[%;**`%EHM34?/B;C3@R**\%EXA94IEH]33):-#E=QB+,EJ/3,YEHM4$BR*0YEE]C3@RU$4O5)[:744,4IYH]!3?Q".Y!A^&gt;+0!%HM!4?!)0X3LQ"*\!%XA#$U-J]!3?Q".!AE%:8E&amp;HQ98"35%1?(C.V2,D,HF)9OT^VVQ8KHI"KB?7?M'I&amp;Y,["KNPH0K'K#&gt;;09(KC6(`90505106![M\6*_I'_^8[E+&gt;K9E[5A&gt;K4_WI\8,J@T\R&gt;LPJ?LXK=LHI@$ZLGC9&gt;DU=&gt;$A@N^XPN&gt;DNNN^P8J^6X^H8&lt;`(YOH4A_P5T5T]XJ'`8S9SXSV`9XK07Z^"[?D@KE_?YRTRT^!E%%/+Q!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="NI_IconEditor" Type="Str">49 55 48 49 56 48 48 54 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 13 3 1 100 1 100 80 84 72 48 0 0 0 29 0 1 0 2 11 77 101 97 115 117 114 101 109 101 110 116 12 72 97 114 100 119 97 114 101 46 112 110 103 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 255 225 184 255 225 184 0 0 0 255 225 184 0 0 0 255 225 184 255 225 184 255 225 184 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 255 225 184 255 225 184 0 0 0 255 225 184 0 0 0 255 225 184 0 0 0 255 225 184 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 0 0 0 0 0 0 0 0 0 255 225 184 0 0 0 255 225 184 0 0 0 255 225 184 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 255 225 184 255 225 184 0 0 0 255 225 184 255 225 184 0 0 0 255 225 184 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 255 225 184 255 225 184 0 0 0 255 225 184 255 225 184 0 0 0 255 225 184 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 0 0 0 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 255 225 184 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 8 1 1

</Property>
	<Item Name="Hardware" Type="Folder">
		<Item Name="Hardware.lvclass" Type="LVClass" URL="../Hardware/Hardware.lvclass"/>
	</Item>
	<Item Name="Class" Type="Folder">
		<Item Name="Connect" Type="Folder">
			<Item Name="Connect.lvclass" Type="LVClass" URL="../Class/Connect/Connect.lvclass"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Configure.lvclass" Type="LVClass" URL="../Class/Configure/Configure.lvclass"/>
		</Item>
		<Item Name="Disconnect" Type="Folder">
			<Item Name="Disconnect.lvclass" Type="LVClass" URL="../Class/Disconnect/Disconnect.lvclass"/>
		</Item>
		<Item Name="BERT" Type="Folder">
			<Item Name="Get Error Data" Type="Folder">
				<Item Name="Get Error Data.lvclass" Type="LVClass" URL="../Class/Get Error Data/Get Error Data.lvclass"/>
			</Item>
			<Item Name="Get Error Data x2" Type="Folder">
				<Item Name="Get Error Data x2.lvclass" Type="LVClass" URL="../Class/Get Error Data x2/Get Error Data x2.lvclass"/>
			</Item>
			<Item Name="Set BERT Order" Type="Folder">
				<Item Name="Set BERT Order.lvclass" Type="LVClass" URL="../Class/Set BERT Order/Set BERT Order.lvclass"/>
			</Item>
			<Item Name="Get BERT Order" Type="Folder">
				<Item Name="Get BERT Order.lvclass" Type="LVClass" URL="../Class/Get BERT Order/Get BERT Order.lvclass"/>
			</Item>
			<Item Name="PAM4 BER Test" Type="Folder">
				<Item Name="PAM4 BER Test.lvclass" Type="LVClass" URL="../Class/PAM4 BER Test/PAM4 BER Test.lvclass"/>
			</Item>
			<Item Name="Get PAM4 FEC" Type="Folder">
				<Item Name="Get PAM4 FEC.lvclass" Type="LVClass" URL="../Class/Get PAM4 FEC/Get PAM4 FEC.lvclass"/>
			</Item>
			<Item Name="Relock" Type="Folder">
				<Item Name="Relock.lvclass" Type="LVClass" URL="../Class/Relock/Relock.lvclass"/>
			</Item>
			<Item Name="Sort BERT By Rx Los" Type="Folder">
				<Item Name="Sort BERT By Rx Los.lvclass" Type="LVClass" URL="../Class/Sort BERT By Rx Los/Sort BERT By Rx Los.lvclass"/>
			</Item>
			<Item Name="Sort BERT 59281" Type="Folder">
				<Item Name="Sort BERT 59281.lvclass" Type="LVClass" URL="../Class/Sort BERT 59281/Sort BERT 59281.lvclass"/>
			</Item>
			<Item Name="PPG Control" Type="Folder">
				<Item Name="PPG Control.lvclass" Type="LVClass" URL="../Class/PPG Control/PPG Control.lvclass"/>
			</Item>
			<Item Name="Check Rx LOS" Type="Folder">
				<Item Name="Check Rx LOS.lvclass" Type="LVClass" URL="../Class/Check Rx LOS/Check Rx LOS.lvclass"/>
			</Item>
			<Item Name="Sort BERT By IP" Type="Folder">
				<Item Name="Sort BERT By IP.lvclass" Type="LVClass" URL="../Class/Sort BERT By IP/Sort BERT By IP.lvclass"/>
			</Item>
		</Item>
		<Item Name="Optical Switch" Type="Folder">
			<Item Name="Switch Channel" Type="Folder">
				<Item Name="Switch Channel.lvclass" Type="LVClass" URL="../Class/Switch Channel/Switch Channel.lvclass"/>
			</Item>
			<Item Name="Sort Optical Switch" Type="Folder">
				<Item Name="Sort Optical Switch.lvclass" Type="LVClass" URL="../Class/Sort Optical Switch/Sort Optical Switch.lvclass"/>
			</Item>
			<Item Name="Second Switch Channel" Type="Folder">
				<Item Name="Second Switch Channel.lvclass" Type="LVClass" URL="../Class/2CH Switch Channel/Second Switch Channel.lvclass"/>
			</Item>
		</Item>
		<Item Name="Power Meter" Type="Folder">
			<Item Name="Get Power" Type="Folder">
				<Item Name="Get Power.lvclass" Type="LVClass" URL="../Class/Get Power/Get Power.lvclass"/>
			</Item>
			<Item Name="Set PM Wavelength" Type="Folder">
				<Item Name="Set PM Wavelength.lvclass" Type="LVClass" URL="../Class/Set PM Wavelength/Set PM Wavelength.lvclass"/>
			</Item>
			<Item Name="Calibration TxP By PM 4CH" Type="Folder">
				<Item Name="Calibration TxP By PM 4CH.lvclass" Type="LVClass" URL="../Class/Calibration TxP By PM 4CH/Calibration TxP By PM 4CH.lvclass"/>
			</Item>
		</Item>
		<Item Name="Power Supply" Type="Folder">
			<Item Name="Get Current" Type="Folder">
				<Item Name="Get Current.lvclass" Type="LVClass" URL="../Class/Get Current/Get Current.lvclass"/>
			</Item>
			<Item Name="Wait Current" Type="Folder">
				<Item Name="Wait Current.lvclass" Type="LVClass" URL="../Class/Wait Current/Wait Current.lvclass"/>
			</Item>
			<Item Name="Check Power State" Type="Folder">
				<Item Name="Check Power State.lvclass" Type="LVClass" URL="../Class/Check Power State/Check Power State.lvclass"/>
			</Item>
			<Item Name="Set Power State" Type="Folder">
				<Item Name="Set Power State.lvclass" Type="LVClass" URL="../Class/Set Power State/Set Power State.lvclass"/>
			</Item>
			<Item Name="Input Voltage Calibration" Type="Folder">
				<Item Name="Input Voltage Calibration.lvclass" Type="LVClass" URL="../Class/Input Voltage Calibration/Input Voltage Calibration.lvclass"/>
			</Item>
			<Item Name="PAM4 CDR" Type="Folder">
				<Item Name="Get Tx Algrethom" Type="Folder">
					<Item Name="Get Tx Algrethom.lvclass" Type="LVClass" URL="../Class/Get Tx Algrethom/Get Tx Algrethom.lvclass"/>
				</Item>
			</Item>
			<Item Name="By Work Order" Type="Folder">
				<Item Name="Wait Current By Work Order" Type="Folder">
					<Item Name="Wait Current By Work Order.lvclass" Type="LVClass" URL="../Class/Wait Current By Work Order/Wait Current By Work Order.lvclass"/>
				</Item>
				<Item Name="Get Current By Work Order" Type="Folder">
					<Item Name="Get Current By Work Order.lvclass" Type="LVClass" URL="../Class/Get Current By Work Order/Get Current By Work Order.lvclass"/>
				</Item>
			</Item>
			<Item Name="Check Power" Type="Folder">
				<Item Name="Check Power.lvclass" Type="LVClass" URL="../Class/Check Power consumption/Check Power.lvclass"/>
			</Item>
		</Item>
		<Item Name="Scope" Type="Folder">
			<Item Name="Get Measurement" Type="Folder">
				<Item Name="Get Measurement.lvclass" Type="LVClass" URL="../Class/Get Measurement/Get Measurement.lvclass"/>
			</Item>
			<Item Name="Verify Measurement" Type="Folder">
				<Item Name="Verify Measurement.lvclass" Type="LVClass" URL="../Class/Verify Measurement/Verify Measurement.lvclass"/>
			</Item>
			<Item Name="Get PAM4 Data" Type="Folder">
				<Item Name="Get PAM4 Data.lvclass" Type="LVClass" URL="../Class/Get PAM4 Data/Get PAM4 Data.lvclass"/>
			</Item>
			<Item Name="Save Screen" Type="Folder">
				<Item Name="Save Screen.lvclass" Type="LVClass" URL="../Class/Save Screen/Save Screen.lvclass"/>
			</Item>
			<Item Name="Waveforms" Type="Folder">
				<Item Name="Waveforms.lvclass" Type="LVClass" URL="../Class/Waveforms/Waveforms.lvclass"/>
			</Item>
			<Item Name="Patterns" Type="Folder">
				<Item Name="Patterns.lvclass" Type="LVClass" URL="../Class/Patterns/Patterns.lvclass"/>
			</Item>
			<Item Name="Tune Power" Type="Folder">
				<Item Name="Tune Power.lvclass" Type="LVClass" URL="../Class/Tune Power/Tune Power.lvclass"/>
			</Item>
			<Item Name="Tune ER" Type="Folder">
				<Item Name="Tune ER.lvclass" Type="LVClass" URL="../Class/Tune ER/Tune ER.lvclass"/>
			</Item>
			<Item Name="Tune PAM4 ER" Type="Folder">
				<Item Name="Tune PAM4 ER.lvclass" Type="LVClass" URL="../Class/Tune PAM4 ER/Tune PAM4 ER.lvclass"/>
			</Item>
			<Item Name="Tune PAM4 TDECQ" Type="Folder">
				<Item Name="Tune PAM4 TDECQ.lvclass" Type="LVClass" URL="../Class/Tune PAM4 TDECQ/Tune PAM4 TDECQ.lvclass"/>
			</Item>
			<Item Name="Refresh Eye Diagram" Type="Folder">
				<Item Name="Refresh Eye Diagram.lvclass" Type="LVClass" URL="../Class/Refresh Eye Diagram/Refresh Eye Diagram.lvclass"/>
			</Item>
		</Item>
		<Item Name="Thermostream" Type="Folder">
			<Item Name="Set Target Temp" Type="Folder">
				<Item Name="Set Target Temp.lvclass" Type="LVClass" URL="../Class/Set Target Temp/Set Target Temp.lvclass"/>
			</Item>
			<Item Name="Run Ramp Temp" Type="Folder">
				<Item Name="Run Ramp Temp.lvclass" Type="LVClass" URL="../Class/Run Ramp Temp/Run Ramp Temp.lvclass"/>
			</Item>
			<Item Name="Temp Calibration By Thermalstream" Type="Folder">
				<Item Name="Temp Calibration By Thermalstream.lvclass" Type="LVClass" URL="../Class/Temp Calibration By Thermalstream/Temp Calibration By Thermalstream.lvclass"/>
			</Item>
		</Item>
		<Item Name="Electrical Scope" Type="Folder">
			<Item Name="Get Electrical Measurement" Type="Folder">
				<Item Name="Get Electrical Measurement.lvclass" Type="LVClass" URL="../Class/Get Electrical Measurement/Get Electrical Measurement.lvclass"/>
			</Item>
			<Item Name="Measurement Electrical Data" Type="Folder">
				<Item Name="Measurement Electrical Data.lvclass" Type="LVClass" URL="../Class/Measurement Electrical Data/Measurement Electrical Data.lvclass"/>
			</Item>
			<Item Name="Electrical Eye Height" Type="Folder">
				<Item Name="Electrical Eye Height.lvclass" Type="LVClass" URL="../Class/Electrical Eye Height/Electrical Eye Height.lvclass"/>
			</Item>
			<Item Name="Electrical Eye Width" Type="Folder">
				<Item Name="Electrical Eye Width.lvclass" Type="LVClass" URL="../Class/Electrical Eye Width/Electrical Eye Width.lvclass"/>
			</Item>
		</Item>
		<Item Name="RF Switch" Type="Folder">
			<Item Name="RF Switch Channel" Type="Folder">
				<Item Name="RF Switch Channel.lvclass" Type="LVClass" URL="../Class/RF Switch Channel/RF Switch Channel.lvclass"/>
			</Item>
			<Item Name="Set RF Switch Order" Type="Folder">
				<Item Name="Set RF Switch Order.lvclass" Type="LVClass" URL="../Class/Set RF Switch Order/Set RF Switch Order.lvclass"/>
			</Item>
			<Item Name="Get RF Switch Order" Type="Folder">
				<Item Name="Get RF Switch Order.lvclass" Type="LVClass" URL="../Class/Get RF Switch Order/Get RF Switch Order.lvclass"/>
			</Item>
			<Item Name="Select A Channel" Type="Folder">
				<Item Name="Select A Channel.lvclass" Type="LVClass" URL="../Class/Select A Channel/Select A Channel.lvclass"/>
			</Item>
			<Item Name="Select B Channel" Type="Folder">
				<Item Name="Select B Channel.lvclass" Type="LVClass" URL="../Class/Select B Channel/Select B Channel.lvclass"/>
			</Item>
		</Item>
		<Item Name="VOA" Type="Folder">
			<Item Name="Set VOA Wavelength" Type="Folder">
				<Item Name="Set VOA Wavelength.lvclass" Type="LVClass" URL="../Class/Set VOA Wavelength/Set VOA Wavelength.lvclass"/>
			</Item>
		</Item>
		<Item Name="Check Connect Class" Type="Folder">
			<Item Name="Check Connect Class.lvclass" Type="LVClass" URL="../Class/Check Connect Class/Check Connect Class.lvclass"/>
		</Item>
		<Item Name="Rx Power Calibration By PM" Type="Folder">
			<Item Name="Rx Power Calibration By PM.lvclass" Type="LVClass" URL="../Class/Rx Power Calibration By PM/Rx Power Calibration By PM.lvclass"/>
		</Item>
	</Item>
</Library>
